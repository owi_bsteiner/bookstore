jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

// We cannot provide stable mock data out of the template.
// If you introduce mock data, by adding .json files in your webapp/localService/mockdata folder you have to provide the following minimum data:
// * At least 3 Author in the list
// * All 3 Author have at least one books

sap.ui.require([
	"sap/ui/test/Opa5",
	"test/cf_crud/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"test/cf_crud/test/integration/pages/App",
	"test/cf_crud/test/integration/pages/Browser",
	"test/cf_crud/test/integration/pages/Master",
	"test/cf_crud/test/integration/pages/Detail",
	"test/cf_crud/test/integration/pages/Create",
	"test/cf_crud/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "test.cf_crud.view."
	});

	sap.ui.require([
		"test/cf_crud/test/integration/MasterJourney",
		"test/cf_crud/test/integration/NavigationJourney",
		"test/cf_crud/test/integration/NotFoundJourney",
		"test/cf_crud/test/integration/BusyJourney",
		"test/cf_crud/test/integration/FLPIntegrationJourney"
	], function () {
		QUnit.start();
	});
});