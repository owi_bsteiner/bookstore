jQuery.sap.require("sap.ui.qunit.qunit-css");
jQuery.sap.require("sap.ui.thirdparty.qunit");
jQuery.sap.require("sap.ui.qunit.qunit-junit");
QUnit.config.autostart = false;

sap.ui.require([
	"sap/ui/test/Opa5",
	"test/cf_crud/test/integration/pages/Common",
	"sap/ui/test/opaQunit",
	"test/cf_crud/test/integration/pages/App",
	"test/cf_crud/test/integration/pages/Browser",
	"test/cf_crud/test/integration/pages/Master",
	"test/cf_crud/test/integration/pages/Detail",
	"test/cf_crud/test/integration/pages/NotFound"
], function (Opa5, Common) {
	"use strict";
	Opa5.extendConfig({
		arrangements: new Common(),
		viewNamespace: "test.cf_crud.view."
	});

	sap.ui.require([
		"test/cf_crud/test/integration/NavigationJourneyPhone",
		"test/cf_crud/test/integration/NotFoundJourneyPhone",
		"test/cf_crud/test/integration/BusyJourneyPhone"
	], function () {
		QUnit.start();
	});
});